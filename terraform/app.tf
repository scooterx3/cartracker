
provider "aws" {
  profile	= "cartracker"
  region     = "us-west-2"
}

resource "aws_dynamodb_table" "cartracker" {
  name           = "cartracker"
  read_capacity  = 20
  write_capacity = 20
  hash_key       = "timestamp"
  range_key      = "licenseplate"


  attribute {
    name = "timestamp"
    type = "S"
  }

  attribute {
    name = "licenseplate"
    type = "S"
  }
}


