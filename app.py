#!/usr/bin/python3

import boto3
import datetime
import getopt
import sys
import random
import string

boto3.setup_default_session(profile_name='cartracker')

ddb = boto3.resource('dynamodb')
table = ddb.Table('cartracker')
timestamp = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")

cmdline_args = sys.argv[1:]

def randomcar():

	# generates random make, model, color and license plate for car
	# returns list in that order (make, model, color, licenseplate) 
	# author: me :D

	# a problem is that it *could* assign the same licenseplate to another car, 
	# but yeah... Maybe I could make a separate table to track *cars*,
	# that way if the randomizer comes up with a license plate number that 
	# already exists in that other table, it can just default back to 
	# that. But now my features are creeping. I think I've fulfilled
	# my purpose already. All I needed was some way to rapid-add to the
	# table in question. 
	
	cars = {
		'Acura':['NSX', 'RDX', 'TLX'], 
		'Ford':['Model-T', 'Explorer', 'Focus'], 
		'Chrysler':['Town and Country','Pacifica', '200'], 
		'Honda':['Odyssey', 'Accord', 'Element'], 
		'Land Rover':['Range Rover','Discovery','LR4'], 
		'Kia':['Sportage', 'Sedona', 'Soul'], 
		'Mercedes':['Sprinter', 'B-class', 'Maybach'], 
		'Nissan':['Titan','Juke','Pathfinder'],
		'Lexus':['RX','LX','RC'],
		'Toyota':['Rav4','Tacoma','Corolla']}

	colors = [
		'blue',
		'green',
		'red',
		'orange',
		'black',
		'white',
		'yellow',
		'purple',
		'rust',
		'dirt']

	LICENSEPLATE_LENGTH = 6

	rand_make = random.choice(list(cars))
	rand_model = random.choice(cars[rand_make])
	rand_color = random.choice(colors)
	rand_licenseplate = ''.join(random.choice(string.ascii_uppercase + 
		string.digits) for _ in range(LICENSEPLATE_LENGTH))

	print("Make: "+rand_make+", Model: "+rand_model+", Color: "+rand_color+
		", Plates: "+rand_licenseplate)

	return rand_make, rand_model, rand_color, rand_licenseplate

def main():

	iterations = 0

	try:
		opt, args = getopt.getopt(cmdline_args, 'i:s:r')
	except getopt.GetoptError as err:
		print("ERROR: ", err)
		sys.exit()

	for item in opt:
		if '-i' in item:
			iterations = int(item[1])

	print(timestamp)
	print(opt)
	print(args)

	if len(opt) == 0:
		print("give me an option")
		sys.exit()

	while iterations > 0:

		car = randomcar()
		table.put_item(
			Item={
				'make':car[0],
				'model':car[1],
				'color':car[2],
				'timestamp':timestamp,
				'licenseplate':car[3]

			})

		iterations = iterations - 1

main()
