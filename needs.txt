What is the need? 

Track each car that goes through a particular intersection. Table contains: 
	make
	model
	color
	timestamp (partition key )
	licenseplate (sort key )

I want a fast way to add a new car to the intersection (manual sure I guess, but make some function that randomly generates one)

	Want to be able to say "run x times" with an option "-i" for iterations. ("-i" also stands for "insert". Cool)

I want to be able to pass an option to grab a range of timestamps (sort them too, jeeze), be able to filter by either make, model or color, or retrieve a specific licenseplate value. 

If licenseplate specified, nothing else is accepted, it just returns whatever it can find from that plate. 

If time range specified, allow for both "start range" and "end range" values. 

I should also be able to filter with -M -m -c (make model color) or --make --model --color. These should work whether I specify time range or no

Need a "-h" option, in case some error is raised when specifying options. 